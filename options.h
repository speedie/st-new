/* speedie.gq's build of suckless.org's simple terminal (st).
 *
 * This is a fork of suckless's st, a minimal in design, just works terminal emulator that by default does nothing more.
 * Note that this terminal has a list of fonts (See 'Font options') that must be changed before installing.
 * 
 * Additionally, if you use OpenBSD or FreeBSD, you must edit 'config.mk'.
 * If you use GNU/Linux, you should be able to recompile st.
 *
 * To find out what to enter in the font and font2 chars, run 'fc-list | grep <fontname>'.
 */

/* Options
 *
 * This configuration file (options.h) contains the many options you can change about this terminal.
 * It is configured in C syntax and needs to be recompiled after a change has been made.
 *
 * You can also configure st using .Xresources.
 * In order to do this, create a .Xresources file somewhere.
 *
 * If you prefer, you can copy 'docs/example.Xresources' somewhere and add 'xrdb /path/to/.Xresources' to a script that auto starts such as .xinitrc.
 * If you use Pywal, it should "just work" out of the box without any additional configuration necessary.
 * 
 * Recompiling is done by running the command 'make clean install' when in the source code directory.
 *
 * If recompiling fails, check config.mk.
 */

/* Modifier key options */
#define MODKEY Mod1Mask
#define TERMMOD (ControlMask|ShiftMask)

static uint forcemousemod        = ShiftMask; /* Force mouse select/shortcuts while mask is active when MODE_MOUSE is set. */
static uint ignoremod            = Mod2Mask|XK_SWITCH_MOD;

/* Font options
 */
static char *font                      = "DejaVuSansMonoForPowerline Nerd Font:style=Book:antialias=true:autohint=true"; /* Main font to use */
/* This font should be bundled with st. Install it manually or with fontctrl (available here: https://codeberg.org/speedie/fontctrl)
 * If it is not available, you may need to 'fontctrl install <font file> --global'.
 */

/* Secondary fonts
 * 
 * These will be picked if the *font does not exist, or doesn't contain the glyphs necessary.
 * You can add multiple fonts if you wish.
 * 
 * For best compatibility, have one Emoji font, one regular font, one Powerline font and one Nerd font.
 * If you don't need these, you can of course omit them.
 */
static char *font2[]                   = { "DejaVu Sans Mono:pixelsize=12:antialias=true:autohint=true",
										   "Noto Emoji:pixelsize=12:antialias=true:autohint=true", 
										   "JoyPixels:pixelsize=12:antialias=true:autohint=true",
										   "fontawesome:pixelsize=12:antialias=true:autohint=true",
};

/* Appearance
 *
 * This is where most appearance related options can be found and changed.
 */

/* Window options */
static int borderpx                 = 0; /* Size of a small border around the text. */
int allowaltscreen                  = 1; /* Allow alt screen (1) */
int allowwindowops                  = 0; /* Allow (insecure) window operations such as setting the clipboard text */

/* Cursor options */

/* List of cursor styles:
 *
 * 0: blinking block
 * 1: blinking block (default)
 * 2: steady block ("█")
 * 3: blinking underline
 * 4: steady underline ("_")
 * 5: blinking bar
 * 6: steady bar ("|")
 * 7: blinking st cursor
 * 8: steady st cursor
 */
static Rune stcursor                = 0x2603; /* snowman ("☃") */
static unsigned int cursorstyle     = 1;
static unsigned int mouseshape      = XC_xterm; /* Shape of the mouse cursor */
static unsigned int mousefg         = 7;
static unsigned int mousebg         = 0;
static unsigned int blinktimeout    = 800; /* Blink timeout for the cursor in milliseconds */
static unsigned int cursorthickness = 2; /* Thickness of the cursor in pixels. */
static unsigned int dctimeout       = 300; /* Double click timeout in milliseconds */
static unsigned int tctimeout       = 600; /* Triple click timeout in milliseconds */
static unsigned int defaultattr     = 11;

/* Text rendering options */
static float cwscale                = 1.0;
static float chscale                = 1.0;
static double minlatency            = 8; /* Minimum draw latency in milliseconds */
static double maxlatency            = 33; /* Max draw latency in milliseconds */
static uint su_timeout              = 200; /* Synchronized timeout in milliseconds */

/* URL options
 * 
 * This build of st allows you to click on links with your mouse.
 * These will then be opened in whatever you define below.
 *
 * This could be a web browser or a clipboard if you prefer to copy the link. 
 *
 * 'xdg-open' is the default but this could be set to 'firefox' or 'chrome'.
 */
static char *url_opener = "xdg-open";

/* Color options */
static const char *colorname[]      = {
    /* Normal colors */
	"#5c5c5c", /* black */
	"#e57373", /* red3 */
	"#02982e", /* green3 */
	"#fac863", /* yellow3 */
	"#6699cc", /* blue2 */
	"#a36ac7", /* magenta3 */
	"#5fb3b3", /* cyan3 */
	"#c0c5ce", /* gray90 */

	/* Bright colors */
	"#00ffaa", /* gray50 */
	"#e57373", /* red */
	"#a6bc69", /* green */
	"#fac863", /* yellow */
	"#6699cc", /* #5c5cff */
    "#c594c5", /* magenta */
	"#5fb3b3", /* cyan */
	"#ffffff", /* white */
	[255] = 0,
	"#cccccc",
	"#555555",
	"#c0c5ce",
	"#696969",
};

/* Default foreground/background colors */
unsigned int defaultfg             = 258; /* Default foreground (text) color */
unsigned int defaultbg             = 232; /* Default background (bg) color */
unsigned int defaultcs             = 256;
static unsigned int defaultrcs     = 257;

/* Alpha options */
float alpha                        = 0.8; /* Background opacity (0 is transparent, 1 is opaque, 0.8 is default) */

/* Boxdraw options
 *
 * 1: render most of the lines/blocks characters without using the font for
 *    perfect alignment between cells (U2500 - U259F except dashes/diagonals).
 *    Bold affects lines thickness if boxdraw_bold is not 0. Italic is ignored.
 * 0: disable (render all U25XX glyphs normally from the font).
 *
 * Boxdraw can cause some compatibility issues so do not enable this unless you want to use it.
 */
const int boxdraw                  = 0; /* Enable (1) or disable boxdraw (0) */
const int boxdraw_bold             = 0; /* Enable (1) or disable bold boxdraw (0) */
const int boxdraw_braille          = 0; /* Render as adjacent "pixels" (1) or use the fonts (0) */

/* Undercurl options
 * 
 * Undercurl allows your terminal and TUI software to draw undercurl instead of an underline.
 * This looks nice to a lot of people.
 *
 * Curly:
 *  _   _   _   _
 * ( ) ( ) ( ) ( )
 *	 (_) (_) (_) (_)
 *
 * To use Curly:
 * Set undercurl to 1
 *
 * Spiky:
 * /\  /\   /\	/\
 *   \/  \/	  \/
 *
 * To use Spiky:
 * Set undercurl to 2
 *
 * Capped:
 *	_     _     _
 * / \   / \   / \
 *    \_/   \_/
 *
 * To use Capped:
 * Set undercurl to 3
 *
 * To use the default (underline), set undercurl to 0.
 */

static char *understyle          = "1"; /* Undercurl style to use */

/* Default column and row options */
static unsigned int cols         = 80; /* Number of columns to have by default */
static unsigned int rows         = 24; /* Number of rows to have by default */

/* Bell options */
static int bellvolume		     = 0; /* Bell volume. It must be a value between -100 and 100. 0 will disable it. */

/* Shell options
 * 
 * This is where most options related to the Shell can be found and changed.
 */
static char *shell               = "/bin/sh";
char *stty_args                  = "stty raw pass8 nl -echo -iexten -cstopb 38400";

/* $TERM options
 * Some software uses the TERM export variable to determine what features the terminal supports.
 * 'st-256color' is the default and will work for most but if you're having compatibility issues
 * try setting it to 'xterm-256color'.
 *
 * You can also set it to 'st-mono' if you want to disable colors completely.
 */
char *termname                   = "st-256color"; /* $TERM value */

/* Tab space options */
unsigned int tabspaces           = 8;
