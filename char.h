/* This header contains options most people probably don't want to change
 * 
 * It is therefore hidden to keep the configuration file easy and clean to read.
 */
char *utmp = NULL;
char *scroll = NULL;
char *vtiden = "\033[?6c";
/* More advanced example: L" `'\"()[]{}" */
wchar_t *worddelimiters = L" ";
static uint selmasks[] = { [SEL_RECTANGULAR] = Mod1Mask };
static char ascii_printable[] = {
	" !\"#$%&'()*+,-./0123456789:;<=>?"
	"@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_"
	"`abcdefghijklmnopqrstuvwxyz{|}~" 
};
