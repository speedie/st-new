/* This header file contains the keybinds available to the user.
 * This is not a complete list, the rest can be changed in array.h and mouse.h respectively.
 */

static Shortcut shortcuts[] = {
	/* mask                  keysym          function        argument */
	{ XK_ANY_MOD,            XK_Break,       sendbreak,      {.i =  0} },
	{ ControlMask,           XK_Print,       toggleprinter,  {.i =  0} },
	{ ShiftMask,             XK_Print,       printscreen,    {.i =  0} },
	{ XK_ANY_MOD,            XK_Print,       printsel,       {.i =  0} },
	{ ControlMask,           XK_equal,       zoom,           {.f = +1} },
	{ ControlMask,           XK_minus,       zoom,           {.f = -1} },
	{ ControlMask,           XK_0,           zoomreset,      {.f =  0} },
	{ ControlMask,           XK_y,           clipcopy,       {.i =  0} },
	{ ControlMask,           XK_p,           clippaste,      {.i =  0} },
	{ TERMMOD,               XK_Num_Lock,    numlock,        {.i =  0} },
	{ ControlMask,           XK_k,           kscrollup,      {.i = +1} },
	{ ControlMask,           XK_j,           kscrolldown,    {.i = +1} },
    { ControlMask|ShiftMask, XK_U,           kscrollup,      {.i = +5} },
    { ControlMask|ShiftMask, XK_D,           kscrolldown,    {.i = +5} },
	{ XK_ANY_MOD,            XK_Page_Up,     kscrollup,      {.i = +20} },
	{ XK_ANY_MOD,            XK_Page_Down,   kscrolldown,    {.i = +20} },
};
